# Аттестационная работа Гафарова А.Ш.
import re
import json
import requests
import asyncio

# 1. Создать класс для хранения параметров погоды.
class Weather:
    """
    класс погода с параметрами city_name, temp, humidity, pressure
   """

    def __init__(self, city_name, temp, humidity, pressure):
        self.city_name = city_name
        self.temp = temp
        self.humidity = humidity
        self.pressure = pressure

# 2. Открыть файл с названием городов, считать города и загрузить в программу список городов.
with open("cities.txt") as file:
    cities = []
    for i in file:
        city = re.findall(r'[A-z]+', i)
        city = ' '.join(city)
        if len(city) > 0:
            cities.append(city)
        else:
            pass

# 3. По считанным городам параллельно (асинхронно) запросить на openweatherAPI информацию о погоде.

async def info_weather(city):
    'запрос информации о погоде'
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    r = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q=%s&appid={openweatherAPI}&units=metric' % (city))
    r = json.loads(r.text)
    city_name = r['name']
    humidity = r['main']['humidity']
    pressure = r['main']['pressure']
    temp = r['main']['temp']
    # print('информация по', city, 'запрошена и сформирована в виде одноименной переменной класса с ожиданием')
    return Weather(city_name, temp, humidity, pressure)


async def info_from_openweatherAPI (cities):
    a = await asyncio.gather(*[info_weather(city) for city in cities])
    return (a)


list_info_from_openweatherAPI = asyncio.run(info_from_openweatherAPI(cities))

for i in list_info_from_openweatherAPI:
    print(f'В городе {i.city_name} \n'
          f'Температура воздуха составит: {i.temp} C° \n'
          f'Влажность воздуха: {i.humidity}%\n'
          f'Давление: {i.pressure} мм.р.т.\n')

